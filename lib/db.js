const { Pool } = require('pg');
const {pg} = global.config;
const uuidv4 = require('uuid/v4');

module.exports = class Pg {

    constructor() {
        this.db = new Pool(pg);
    }

    async query(sql, values) {
        let res;
        try {
            res = await this.db.query(sql, values);
        } catch(e) {
            console.log(e);
            console.log('sql:', sql);
            console.log('values:', values);
        }
        return res;
    }

    async checkSchema(name) {
        const {rows} = await this.query('select schema_name from information_schema.schemata where schema_name=$1', [name])
        return !!rows.length;
    }

    async createSchema(name) {
        await this.query(`create schema ${name}`);
    }

    async checkTable(name) {
        const {rows} = await this.query('SELECT * FROM information_schema.tables WHERE table_schema = $1 AND table_name = $2', [pg.schema, name]);
        return !!rows.length;
    }
    
    async createTable(name, fields, uniqueIndex) {
        let fieldsArr = [];
        for(let k in fields) {
            fieldsArr.push(`${k} ${fields[k]}`);
        }        
        await this.query(`create table ${pg.schema}.${name} (${fieldsArr.join(',')})`);
        if(uniqueIndex) {
            await this.query(`CREATE UNIQUE INDEX uq_${name} on ${pg.schema}.${name} (${uniqueIndex.join(',')})`);
        }
    }

    async upsert(table, data) {
        const uuid = uuidv4();
        let insFields = ['_id'], updFields = [], insValuesNumbers = ['$1'], updValuesNumbers = [], values = [uuid];

        Object.keys(data).forEach((k,i) => {
            insFields.push(k);
            updFields.push(k);
            insValuesNumbers.push(`$${i+2}`);
            updValuesNumbers.push(`$${i+2}`);
            values.push(data[k]);
        })

        const {rows} = await this.query(`INSERT INTO ${table} (${insFields}) VALUES (${insValuesNumbers}) 
        ON CONFLICT (src, id) DO 
        UPDATE SET (${updFields}) = (${updValuesNumbers})
        RETURNING ${table}.*`, values);

        if(rows && rows[0])
            return  rows[0];                 

        return null;
    }

    async findOne(table, conditions, fields) {
        let values = [], where = [];
        Object.keys(conditions).forEach((k,i) => {
            where.push(`${k} = $${i+1}`);
            values.push(conditions[k]);
        });
        const {rows} = await this.query(`SELECT ${fields} FROM ${table} WHERE ${where.join(' and ')}`, values);
        return rows && rows[0]? rows[0]:null;
    }


}
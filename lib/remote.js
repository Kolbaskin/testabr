const axios = require('axios');

module.exports = class Remote {

    async getAllData(url, method, handler) {
        let res;
        do {
            res = await this.readData(url, method, res, handler);
        } while(res);
    }

    async readData(url, method, after, handler) {
        let params = {
            method,
            first: global.config.recordsPerStep
        }
        if(after)
            params.after = after;       
            
        const {data} = await axios.get(url, {
            params,
            responseType: 'json'
        })
        if(data && data.count) {
            for(let i=0;i<data.items.length;i++) {
                await handler(data.items[i]);
            }
            return data.lastItemCursor;
        } else
            return;
    }
}
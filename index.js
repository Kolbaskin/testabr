global.config = require('config');
const Pg = require('./lib/db');
const Remote = require('./lib/remote');
const localDb = new Pg();
const remoteSrv = new Remote();
const {pg} = global.config;

class main {

    async run() {
        const startTime = Date.now();

        await this.doSchema();
        await this.doTables();
        await this.doData();

        console.log('Duration (ms):', Date.now() - startTime);
    }

    // проверяем наличие / создаем схему
    async doSchema() {    
        const issetSchema = await localDb.checkSchema(pg.schema);
        if(issetSchema) {
            console.log(`Schema ${pg.schema} already exists.`)
        } else {
            await localDb.createSchema(pg.schema);
            console.log(`Schema ${pg.schema} is created.`)
        }
        return true;
    }

    // проверяем / создаем одну таблицу
    async doTable(name, fields, uniqueIndex) {
        const isset = await localDb.checkTable(name);
        if(isset) {
            console.log(`Table "${name}" already exists.`)
        } else {
            await localDb.createTable(name, fields, uniqueIndex)
            console.log(`Table "${name}" is created.`)
        }
    }

    // проверяем / создаем таблицы
    async doTables() {
        await this.doTable('equipment', {
            "_id": "uuid",
            "src": "varchar(100)",
            "id":"int",
            "name":"varchar(255)",
            "price":"real",
            "type":"uuid"
        }, ['src', 'id']);
        
        await this.doTable('types', {
            "_id": "uuid",
            "src": "varchar(100)",
            "id":"int",
            "name":"varchar(255)",
            "warrantyperiod":"uuid"
        }, ['src', 'id']);

        await this.doTable('warrantyperiod', {
            "_id": "uuid",
            "src": "varchar(100)",
            "id":"int",
            "name":"varchar(255)",
            "period":"date"
        }, ['src', 'id']);
    }

    // сохраняем тип
    async saveTypeRecord(src, data) {
        const res = await localDb.upsert(pg.schema + '.warrantyperiod', {
            src,
            id: data.warrantyPeriodByWarrantyPeriod.id,
            name: data.warrantyPeriodByWarrantyPeriod.name,
            period: data.warrantyPeriodByWarrantyPeriod.period
        });
        
        await localDb.upsert(pg.schema + '.types', {
            src,
            id: data.id,
            name: data.name,
            warrantyperiod: res? res._id:null
        });
    }

    // сохраняем запись с девайсом
    async saveEquipmentRecord(src, data) {
        // нужно связать с локальным типом, найдем его
        const type = await localDb.findOne(pg.schema + '.types', {
            src,
            id: data.type
        }, ['_id']);
        await localDb.upsert(pg.schema + '.equipment', {
            src,
            id: data.id,
            name: data.name,
            price: data.price,
            type: type._id
        });
    }

    // обработка внешних данных для типов
    async doTypesData(serverCfg) {
        await remoteSrv.getAllData(serverCfg.url, 'getTypes', async (data) => {
            await this.saveTypeRecord(serverCfg.name, data)
        })
    }

    // обработка внешних данных для девайсов
    async doEquipmentData(serverCfg) {
        await remoteSrv.getAllData(serverCfg.url, 'getEquipment', async (data) => {
            await this.saveEquipmentRecord(serverCfg.name, data)
        })
    }

    // перебираем сервера
    async doData() {
        for(let i = 0;i<global.config.remote_servers.length;i++) {
            await this.doTypesData(global.config.remote_servers[i]);
            await this.doEquipmentData(global.config.remote_servers[i]);
        }
    }
}


new main().run();
